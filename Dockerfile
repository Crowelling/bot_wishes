FROM node:17.0.1
WORKDIR /app
COPY ["package.json", "package-lock.json*", "/app"]
RUN npm install
COPY . /app
EXPOSE 8080
CMD ["npm", "start"]