export const token = '5003503778:AAHz8tjXwG80XW94AuXrPRk829rwHjH876c'

export const wishLists = {
    'Aleksey': {
        command: '/lesha_wish_list',
        description: 'OMG! What Lesha wants again?!',
        сategories: {},
    },
    'Maria': {
        command: '/masha_wish_list',
        description: 'What Masha wants?',
        сategories: {},
    },
}

export const categories = {
    '/interior': 'Интерьер',
    '/exterior': 'Экстерьер',
    '/tourism': 'Путешествия',
    '/hobby': 'Хобби',
    '/totry': 'Испытать',
    '/food': 'Еда',
}

export const categoriesCallback = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{text: 'Интерьер', callback_data: '/interior'}, {text: 'Экстерьер', callback_data: '/exterior'}],
            [{text: 'Путешествия', callback_data: '/tourism'}, {text: 'Хобби', callback_data: '/hobby'}],
            [{text: 'Испытать', callback_data: '/totry'}, {text: 'Еда', callback_data: '/food'}],
        ]
    })
}
