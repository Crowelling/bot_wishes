import TelegramBot from 'node-telegram-bot-api'
import { MongoClient } from 'mongodb'

import { token, wishLists, categoriesCallback, categories } from './options.js'


let text = ''
let requestedName = ''
const bot = new TelegramBot(token, { polling: true })
const chatIds = new Set()


const answerAll = (text) => {
    chatIds.forEach(chatId => {
        bot.sendMessage(chatId, text)
    })
}

const start = async () => {
    // Connection to DB
    const url = 'mongodb://mongo:27017'
    const client = new MongoClient(url)
    const dbName = 'WishesBot'
    await client.connect()
    console.log('Connected successfully to DB')
    const wishListsDB = client.db(dbName).collection('wishLists')

    // set commands for client
    bot.setMyCommands(Object.values(wishLists))

    bot.on('message', msg => {
        text = msg.text
        const chatId = msg.chat.id

        chatIds.add(chatId)

        try {
            if (text === '/lesha_wish_list' || text === '/masha_wish_list') {
                requestedName = Object.keys(wishLists).filter(name => wishLists[name].command === text)[0]

                return bot.sendMessage(chatId, 'Хотелки из какой категории?', categoriesCallback)
            } 
            else if (text === '/start') {
                return bot.sendMessage(chatId, 'Добро пожаловать! Просто пиши мне сообщения, и я их сохраню в твой список желаний!')
            } else {
                bot.sendMessage(chatId, 'В какую категорию добавить?', categoriesCallback)
            }
        } catch (e) {
            console.error(e)
            return answerAll('Шота пошло не так! Спроси у Леши)')
        }

    })

    bot.on('callback_query', async msg => {
        const filter = msg.message.text
        const category = msg.data
        const { id: chatId, first_name: userName } = msg.message.chat
        
        switch(filter) {
            case 'В какую категорию добавить?':
                text = `♥ ${text}`

                await wishListsDB.insertOne({ userName, category, wish: text })

                // удаляем из чата меню выбора категории
                bot.deleteMessage(chatId, msg.message.message_id)

                return bot.sendMessage(
                    chatId,
                    `Добавлено в категорию *${categories[category]}*!`,
                    { parse_mode: 'Markdown' },
                )

            case 'Хотелки из какой категории?':
                const wishListCursors = wishListsDB.find({ userName: requestedName, category })
                const wishList = (await wishListCursors.count())
                    ? (await wishListCursors.toArray()).map(el => el.wish)
                    : null

                if (wishList) {
                    await bot.sendMessage(
                        chatId,
                        `*${categories[category]}*:`,
                        { parse_mode: 'Markdown' },
                    )

                    return bot.sendMessage(chatId, wishList.join('\n'))
                } else {
                    if (userName !== requestedName) {
                        return bot.sendMessage(
                            chatId,
                            `Хм, а в категории *${categories[category]}* у ${requestedName} ничего нет :-(`,
                            { parse_mode: 'Markdown' },
                        )
                    } else {
                        return bot.sendMessage(
                            chatId,
                            `А в *${categories[category]}* пусто. Добавь что-то!`,
                            { parse_mode: 'Markdown' },
                        )
                    }
                }
        }
    })
}

start()
